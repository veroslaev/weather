<?php

declare(strict_types=1);

namespace Eroslaev\Weather\Service;

use Eroslaev\Weather\Api\Data\WeatherInterfaceFactory;
use Eroslaev\Weather\Api\WeatherRepositoryInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Cache\FrontendInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\HTTP\Client\Curl;
use Magento\Framework\Serialize\SerializerInterface;

class WeatherApi
{
    private const XML_PATH_CONNECTION_API = 'eroslaev_weather/connection/api';
    private const XML_PATH_CONNECTION_CITY = 'eroslaev_weather/connection/city';
    private const XML_PATH_CONNECTION_APPID = 'eroslaev_weather/connection/appid';
    private const XML_PATH_CONNECTION_UNITS = 'eroslaev_weather/connection/units';

    private WeatherInterfaceFactory $weatherInterfaceFactory;
    private WeatherRepositoryInterface $weatherRepository;
    private Curl $curl;
    private ScopeConfigInterface $scopeConfig;
    private SerializerInterface $serializer;

    public function __construct(
        WeatherInterfaceFactory $weatherInterfaceFactory,
        WeatherRepositoryInterface $weatherRepository,
        Curl $curl,
        ScopeConfigInterface $scopeConfig,
        SerializerInterface $serializer
    ) {
        $this->weatherInterfaceFactory = $weatherInterfaceFactory;
        $this->weatherRepository = $weatherRepository;
        $this->scopeConfig = $scopeConfig;
        $this->curl = $curl;
        $this->serializer = $serializer;
    }

    /**
     * @throws LocalizedException
     */
    public function saveWeather(): void
    {
        $urlApi = $this->scopeConfig->getValue(self::XML_PATH_CONNECTION_API);
        $urlCity = $this->scopeConfig->getValue(self::XML_PATH_CONNECTION_CITY);
        $urlAppId = $this->scopeConfig->getValue(self::XML_PATH_CONNECTION_APPID);
        $urlUnits = $this->scopeConfig->getValue(self::XML_PATH_CONNECTION_UNITS);
        if (empty($urlApi) || empty($urlCity) || empty($urlAppId)) {
            throw new LocalizedException(__('Connection settings are empty. Please fill settings'));
        }
        $url = $urlApi . '?q=' . $urlCity . '&appid=' . $urlAppId;
        if (!empty($urlUnits)) {
            $url .= '&units=' . $urlUnits;
        }
        $this->curl->get($url);
        $response = $this->curl->getBody();
        if (!$response) {
            throw new LocalizedException(__('Something wrong when execute GET query to weather service api'));
        }
        $data = $this->serializer->unserialize($response);
        $weather = $this->weatherInterfaceFactory->create();
        $weather->setCity($data['name']);
        $weather->setTemperature($data['main']['temp']);
        $weather->setMaxTemperature($data['main']['temp_max']);
        $weather->setMinTemperature($data['main']['temp_min']);
        $weather->setFullJson($response);
        $this->weatherRepository->save($weather);
    }
}
