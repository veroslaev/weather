<h2>Eroslaev_Weather module</h2>

## Overview

The Eroslaev_Weather module implements the integration with the Weather API

## Implementation Details

Plans: phtml template rework to ko and ui component for auto-refresh each time

1.0.0
- Model + Resource model + Collection + Interfaces
- Service for cURL, get weather info and save in db
- Cron with settings in adminhtml
- Frontend showing in footer
