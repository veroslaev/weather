<?php

declare(strict_types=1);

namespace Eroslaev\Weather\Block;

use Eroslaev\Weather\Model\ResourceModel\Weather\Collection;
use Magento\Framework\View\Element\Template;

class Weather extends Template
{
    private Collection $collection;

    public const CACHE_TAG = 'eroslaev_weather_block';

    public function __construct(Template\Context $context, Collection $collection, array $data = [])
    {
        parent::__construct($context, $data);
        $this->collection = $collection;
    }

    public function checkWeather(): bool
    {
        return (bool)($this->_scopeConfig->getValue('eroslaev_weather/general/is_enabled'));
    }

    public function getTemps(): array
    {
        return $this->collection->getLastItem()->toArray();
    }
}
