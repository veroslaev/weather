<?php

declare(strict_types=1);

namespace Eroslaev\Weather\Cron;

use Eroslaev\Weather\Service\WeatherApi;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Psr\Log\LoggerInterface;

class AddHistory
{
    private WeatherApi $weatherApi;
    private ScopeConfigInterface $scopeConfig;
    private LoggerInterface $logger;

    public function __construct(WeatherApi $weatherApi, LoggerInterface $logger, ScopeConfigInterface $scopeConfig)
    {
        $this->weatherApi = $weatherApi;
        $this->scopeConfig = $scopeConfig;
        $this->logger = $logger;
    }

    public function execute(): void
    {
        if ($this->scopeConfig->getValue('eroslaev_weather/general/is_enabled')) {
            try {
                $this->weatherApi->saveWeather();
            } catch (\Exception $e) {
                $this->logger->critical($e->getMessage());
            }
        }
    }
}
