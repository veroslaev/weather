<?php

declare(strict_types=1);

namespace Eroslaev\Weather\Model\ResourceModel\Weather;

use Eroslaev\Weather\Model\Weather;
use Eroslaev\Weather\Model\ResourceModel\Weather as WeatherResource;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * @inheritdoc
     */
    // @codingStandardsIgnoreStart
    protected $_eventPrefix = 'eroslaev_weather_weather_collection';

    /**
     * @inheritdoc
     */
    protected $_eventObject = 'object';

    /**
     * @inheritdoc
     * @throws LocalizedException
     */
    protected function _construct()
    {
        $this->_init(Weather::class, WeatherResource::class);
        $this->_setIdFieldName($this->getResource()->getIdFieldName());
    }
    // @codingStandardsIgnoreStart
}
