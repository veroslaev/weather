<?php

declare(strict_types=1);

namespace Eroslaev\Weather\Model\ResourceModel;

use Eroslaev\Weather\Api\Data\WeatherInterface;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Weather extends AbstractDb
{
    // @codingStandardsIgnoreStart
    protected $_idFieldName = WeatherInterface::ID;

    /**
     * @inheritDoc
     */
    protected function _construct()
    {
        $this->_init(WeatherInterface::TABLE, WeatherInterface::ID);
    }
    // @codingStandardsIgnoreStart
}
