<?php

declare(strict_types=1);

namespace Eroslaev\Weather\Model\Source\Weather;

use Magento\Framework\Data\OptionSourceInterface;

class MeasurementUnit implements OptionSourceInterface
{
    private const STANDARD = 'standard';
    private const METRIC = 'metric';
    private const IMPERIAL = 'imperial';

    /**
     * @return array[]
     */
    public function toOptionArray(): array
    {
        return [
            ['value' => self::STANDARD, 'label' => __('Standard')],
            ['value' => self::METRIC, 'label' => __('Metric')],
            ['value' => self::IMPERIAL, 'label' => __('Imperial')]
        ];
    }
}
