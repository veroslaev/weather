<?php

declare(strict_types=1);

namespace Eroslaev\Weather\Model;

use Eroslaev\Weather\Api\Data\WeatherInterface;
use Magento\Framework\Model\AbstractModel;

class Weather extends AbstractModel implements WeatherInterface
{
    // @codingStandardsIgnoreStart
    protected $_idFieldName = 'entity_id';

    protected function _construct()
    {
        $this->_init(ResourceModel\Weather::class);
    }
    // @codingStandardsIgnoreEnd

    public function getId(): ?int
    {
        return $this->getData(self::ID);
    }

    public function setCity(string $city): void
    {
        $this->setData(self::CITY, $city);
    }

    public function getCity(): string
    {
        return $this->getData(self::CITY);
    }

    public function setTemperature(float $temp): void
    {
        $this->setData(self::TEMPERATURE, $temp);
    }

    public function getTemperature(): float
    {
        return $this->getData(self::TEMPERATURE);
    }

    public function setMinTemperature(float $temp): void
    {
        $this->setData(self::TEMPERATURE_MIN, $temp);
    }

    public function getMinTemperature(): float
    {
        return $this->getData(self::TEMPERATURE_MIN);
    }

    public function setMaxTemperature(float $temp): void
    {
        $this->setData(self::TEMPERATURE_MAX, $temp);
    }

    public function getMaxTemperature(): float
    {
        return $this->getData(self::TEMPERATURE_MAX);
    }

    public function getFullJson(): string
    {
        return $this->getData(self::FULL_JSON);
    }

    public function setFullJson(string $json): void
    {
        $this->setData(self::FULL_JSON, $json);
    }

    public function setCreatedAt(string $created): void
    {
        $this->setData(self::CREATED_AT, $created);
    }

    public function getCreatedAt(): string
    {
        return $this->getData(self::CREATED_AT);
    }
}
