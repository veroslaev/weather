<?php

namespace Eroslaev\Weather\Model;

use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\CouldNotDeleteException;
use Eroslaev\Weather\Api\Data\WeatherInterface;
use Eroslaev\Weather\Api\Data\WeatherInterfaceFactory;
use Eroslaev\Weather\Api\Data\WeatherSearchResultsInterfaceFactory;
use Eroslaev\Weather\Api\WeatherRepositoryInterface;
use Eroslaev\Weather\Model\ResourceModel\Weather as WeatherResource;
use Eroslaev\Weather\Model\ResourceModel\Weather\CollectionFactory;

class WeatherRepository implements WeatherRepositoryInterface
{
    /** @var CollectionFactory */
    private CollectionFactory $collectionFactory;

    /** @var WeatherInterfaceFactory */
    private WeatherInterfaceFactory $entityFactory;

    /** @var WeatherResource */
    private WeatherResource $resource;

    /** @var WeatherSearchResultsInterfaceFactory */
    private WeatherSearchResultsInterfaceFactory $searchResultFactory;

    /**
     * WeatherRepository constructor.
     *
     * @param WeatherResource $resource
     * @param WeatherInterfaceFactory $itemsFactory
     * @param CollectionFactory     $collectionFactory
     * @param WeatherSearchResultsInterfaceFactory     $searchResultFactory
     */
    public function __construct(
        WeatherResource $resource,
        WeatherInterfaceFactory $itemsFactory,
        CollectionFactory $collectionFactory,
        WeatherSearchResultsInterfaceFactory $searchResultFactory
    ) {
        $this->resource = $resource;
        $this->entityFactory = $itemsFactory;
        $this->collectionFactory = $collectionFactory;
        $this->searchResultFactory = $searchResultFactory;
    }

    /**
     * @inheritdoc
     */
    public function save(WeatherInterface $item) : WeatherInterface
    {
        try {
            $this->resource->save($item);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }

        return $item;
    }

    /**
     * @inheritdoc
     */
    public function getById(int $itemId) : WeatherInterface
    {
        $items = $this->entityFactory->create();
        $this->resource->load($items, $itemId);
        if (!$items->getId()) {
            throw new NoSuchEntityException(__('Item with id "%1" does not exist.', $itemId));
        }

        return $items;
    }

    /**
     * @inheritdoc
     */
    public function findById($itemId): ?WeatherInterface
    {
        $items = $this->entityFactory->create();
        $this->resource->load($items, $itemId);

        if (!$items->getId()) {
            return null;
        }

        return $items;
    }

    /**
     * @inheritdoc
     */
    public function getList(SearchCriteriaInterface $searchCriteria): SearchCriteriaInterface
    {
        $searchResult = $this->searchResultFactory->create();
        $searchResult->setSearchCriteria($searchCriteria);
        $collection = $this->collectionFactory->create();

        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                $condition = $filter->getConditionType() ?: 'eq';
                $collection->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
            }
        }

        $sortOrders = $searchCriteria->getSortOrders();
        $searchResult->setTotalCount($collection->getSize());
        if ($sortOrders) {
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() === SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        return $searchResult->setWeather($collection->getWeather());
    }

    /**
     * @inheritdoc
     */
    public function delete(WeatherInterface $items): bool
    {
        try {
            $this->resource->delete($items);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function deleteById(int $itemId): bool
    {
        return $this->delete($this->getById($itemId));
    }
}
