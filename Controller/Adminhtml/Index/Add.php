<?php

namespace Eroslaev\Weather\Controller\Adminhtml\Index;

use Eroslaev\Weather\Service\WeatherApi;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\View\Result\Page;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\HttpGetActionInterface;

class Add extends Action implements HttpGetActionInterface
{
    private PageFactory $pageFactory;
    private WeatherApi $weatherApi;

    /**
     * Constructor
     *
     * @param Context $context
     * @param PageFactory $rawFactory
     * @param WeatherApi $weatherApi
     */
    public function __construct(
        Context $context,
        PageFactory $rawFactory,
        WeatherApi $weatherApi
    ) {
        $this->pageFactory = $rawFactory;
        $this->weatherApi = $weatherApi;

        parent::__construct($context);
    }

    /**
     * Add the main Admin Grid page
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute(): \Magento\Backend\Model\View\Result\Redirect
    {
        try {
            $this->weatherApi->saveWeather();
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        }
        //$resultPage->setActiveMenu('Eroslaev_Weather::home');
        //$resultPage->getConfig()->getTitle()->prepend(__('Weather History'));

        return $this->resultFactory->create(ResultFactory::TYPE_REDIRECT)->setPath('eroslaev_weather/index/index');
    }
}
