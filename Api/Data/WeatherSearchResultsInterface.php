<?php

declare(strict_types=1);

namespace Eroslaev\Weather\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface WeatherSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get Items list.
     *
     * @return WeatherInterface[]
     */
    public function getWeather(): array;

    /**
     * Set Items list.
     *
     * @param WeatherInterface[] $items
     *
     * @return $this
     */
    public function setWeather(array $items): SearchResultsInterface;
}
