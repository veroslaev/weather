<?php

declare(strict_types=1);

namespace Eroslaev\Weather\Api\Data;

interface WeatherInterface
{
    public const ID = 'entity_id';
    public const CITY = 'city';
    public const TEMPERATURE = 'temperature';
    public const TEMPERATURE_MIN = 'temperature_min';
    public const TEMPERATURE_MAX = 'temperature_max';
    public const FULL_JSON = 'full_json';
    public const CREATED_AT = 'created_at';

    public const TABLE = 'eroslaev_weather_history';

    public function getId(): ?int;

    public function setCity(string $city): void;

    public function getCity(): string;

    public function setTemperature(float $temp): void;

    public function getTemperature(): float;

    public function setMinTemperature(float $temp): void;

    public function getMinTemperature(): float;

    public function setMaxTemperature(float $temp): void;

    public function getMaxTemperature(): float;

    public function getFullJson(): string;

    public function setFullJson(string $json): void;

    public function setCreatedAt(string $created): void;

    public function getCreatedAt(): string;
}
