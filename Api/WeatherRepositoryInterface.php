<?php

declare(strict_types=1);

namespace Eroslaev\Weather\Api;

use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Eroslaev\Weather\Api\Data\WeatherInterface;
use Eroslaev\Weather\Api\Data\WeatherSearchResultsInterface;

interface WeatherRepositoryInterface
{
    /**
     * Save Items
     *
     * @param WeatherInterface $item
     *
     * @return WeatherInterface
     * @throws CouldNotSaveException
     */
    public function save(WeatherInterface $item): WeatherInterface;

    /**
     * Get Items by id.
     *
     * @param int $itemId
     *
     * @return WeatherInterface
     * @throws NoSuchEntityException
     */
    public function getById(int $itemId): WeatherInterface;

    /**
     * Find Items by id.
     *
     * @param int $itemId
     *
     * @return WeatherInterface|null
     */
    public function findById(int $itemId): ?WeatherInterface;

    /**
     * Retrieve Items matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return SearchCriteriaInterface
     * @throws LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria): SearchCriteriaInterface;

    /**
     * Delete Items
     *
     * @param WeatherInterface $items
     *
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(WeatherInterface $items): bool;

    /**
     * Delete Items by ID.
     *
     * @param int $itemId
     *
     * @return bool
     * @throws NoSuchEntityException
     * @throws CouldNotDeleteException
     */
    public function deleteById(int $itemId): bool;
}
